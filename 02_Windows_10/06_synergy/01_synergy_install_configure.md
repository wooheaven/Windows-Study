# download 
[free stable version](https://sourceforge.net/projects/synergy-stable-builds/files/v1.8.8-stable/)

# configure server
![Server](02_synergy_Server.png)  

# configure client
![Client](03_synergy_Client.png)  

