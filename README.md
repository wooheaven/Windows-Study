╠═1 Windows7  
║░╠═1 font  
║░║░╚═1 [font location](01_Windows_7/01_font/01_font_location.md)  
║░╠═2 git  
║░║░╠═1 [Install vim bundle : Vundle](01_Windows_7/02_git/01_install_vim_bundle_Vundle_on_git-bash.md)  
║░║░╠═2 [Install vim bundle : NerdTree](01_Windows_7/02_git/02_install_vim_bundle_NERD_Tree_on_git-bash.md)  
║░║░╚═3 [Install rsync on git-bash](01_Windows_7/02_git/03_install_rsync_to_git-bash.md)  
║░╠═3 network drive  
║░║░╚═1 [connect network drive](01_Windows_7/03_network_drive/01_connect_network_drive.md)  
║░╚═4 update  
║░░░╚═1 [disable update](01_Windows_7/04_update/01_disable_update.md)  
╠═2 Windows10  
║░╠═1 cmd  
║░║░╚═1 [cmd : redirect](02_Windows_10/01_cmd/01_redirect.md)  
║░╠═2 [hosts](02_Windows_10/02_hosts/01_hosts.md)  
║░╠═3 [md5](02_Windows_10/03_md5/01_md5_on_windows10.md)  
║░╠═4 [mouse : activate](02_Windows_10/04_mouse/01_activate_mouse.md)  
║░╠═5 [remote desktop](02_Windows_10/05_remote_desktop/01_activate_remote_desktop.md)  
║░╠═6 [synergy](02_Windows_10/06_synergy/01_synergy_install_configure.md)  
║░╚═7 [Time : internet time sync](02_Windows_10/07_time/01_internet_time_sync.md)  
╠═99 Utility  
║░╠═1 [modify renamed files on README.md](99_Utility/03_modify_number_of_file_on_README.sh)  
║░╚═2 [lists of renamed files](99_Utility/change_A_to_B.txt)  
╚═README.md  
